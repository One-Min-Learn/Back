import React from 'react';

export default function EditorCell({ cellData }) {
  if (!cellData) return <span>{`<No Content>`}</span>;
  return <span>{cellData.length > 140 ? `${cellData.substring(0, 140)}...` : cellData}</span>;
}
