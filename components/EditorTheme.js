/**
 * Rich MD Editor Colors
 */
const colors = {
  almostBlack: '#181a1b',
  lightBlack: '#2f3336',
  almostWhite: '#e6e6e6',
  white: '#fff',
  white10: 'rgba(255, 255, 255, 0.1)',
  black: '#000',
  black10: 'rgba(0, 0, 0, 0.1)',
  primary: '#1ab6ff',
  greyLight: '#f4f7fa',
  grey: '#e8ebed',
  greyMid: '#c5ccd3',
  greyDark: '#dae1e9'
};

/**
 * Rich MD Editor Base Theme
 */
export const baseTheme = {
  ...colors,

  // prettier-ignore
  fontFamily: "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
  fontFamilyMono: "'SFMono-Regular', Consolas, 'Liberation Mono', Menlo, Courier, monospace",
  fontWeight: 400,
  zIndex: 100,
  link: colors.primary,
  placeholder: '#b1becc',
  textSecondary: '#4e5c6e',
  textLight: colors.white,
  textHighlight: '#b3e7ff',
  textHighlightForeground: colors.black,
  selected: colors.primary,
  codeComment: '#6a737d',
  codePunctuation: '#5e6687',
  codeNumber: '#d73a49',
  codeProperty: '#c08b30',
  codeTag: '#3d8fd1',
  codeString: '#032f62',
  codeSelector: '#6679cc',
  codeAttr: '#c76b29',
  codeEntity: '#22a2c9',
  codeKeyword: '#d73a49',
  codeFunction: '#6f42c1',
  codeStatement: '#22a2c9',
  codePlaceholder: '#3d8fd1',
  codeInserted: '#202746',
  codeImportant: '#c94922',

  blockToolbarBackground: colors.white,
  blockToolbarTrigger: colors.greyMid,
  blockToolbarTriggerIcon: colors.white,
  blockToolbarItem: colors.almostBlack,
  blockToolbarIcon: undefined,
  blockToolbarIconSelected: colors.black,
  blockToolbarText: colors.almostBlack,
  blockToolbarTextSelected: colors.black,
  blockToolbarSelectedBackground: colors.greyMid,
  blockToolbarHoverBackground: colors.greyLight,
  blockToolbarDivider: colors.greyMid,

  noticeInfoBackground: '#f5be31',
  noticeInfoText: colors.almostBlack,
  noticeTipBackground: '#9e5cf7',
  noticeTipText: colors.white,
  noticeWarningBackground: '#ff5c80',
  noticeWarningText: colors.white
};

/**
 * Rich MD Editor Default Light Theme
 */
export const lightTheme = {
  ...baseTheme,

  background: colors.white,
  text: colors.almostBlack,
  code: colors.lightBlack,
  cursor: colors.black,
  divider: colors.greyMid,

  toolbarBackground: colors.lightBlack,
  toolbarHoverBackground: colors.black,
  toolbarInput: colors.white10,
  toolbarItem: colors.white,

  tableDivider: colors.greyMid,
  tableSelected: colors.primary,
  tableSelectedBackground: '#e5f7ff',

  quote: colors.greyDark,
  codeBackground: colors.greyLight,
  codeBorder: colors.grey,
  horizontalRule: colors.greyMid,
  imageErrorBackground: colors.greyLight,

  scrollbarBackground: colors.greyLight,
  scrollbarThumb: colors.greyMid
};

/**
 * Rich MD Editor Default Dark Theme
 */
export const darkTheme = {
  ...baseTheme,

  background: colors.almostBlack,
  text: colors.almostWhite,
  code: colors.almostWhite,
  cursor: colors.white,
  divider: '#4e5c6e',
  placeholder: '#52657a',

  toolbarBackground: colors.white,
  toolbarHoverBackground: colors.greyMid,
  toolbarInput: colors.black10,
  toolbarItem: colors.lightBlack,

  tableDivider: colors.lightBlack,
  tableSelected: colors.primary,
  tableSelectedBackground: '#002333',

  quote: colors.greyDark,
  codeBackground: colors.black,
  codeBorder: colors.lightBlack,
  codeString: '#3d8fd1',
  horizontalRule: colors.lightBlack,
  imageErrorBackground: 'rgba(0, 0, 0, 0.5)',

  scrollbarBackground: colors.black,
  scrollbarThumb: colors.lightBlack
};

/**
 * Custom Theme (Based On PayloadCMS)
 */
const editorTheme = {
  ...baseTheme,

  fontFamily: 'var(--font-body)',
  fontFamilyMono: 'var(--font-mono)',
  link: 'var(--theme-success-500)',
  placeholder: 'var(--theme-elevation-400)',

  background: 'var(--theme-input-bg)',
  text: 'var(--theme-text)',
  code: 'var(--theme-text)',
  cursor: 'var(--theme-text)',
  divider: 'var(--theme-elevation-400)',

  toolbarBackground: 'var(--theme-elevation-800)',
  toolbarHoverBackground: 'var(--theme-elevation-750)',
  toolbarInput: 'transparent',
  toolbarItem: 'var(--theme-elevation-0)',

  tableDivider: 'var(--theme-elevation-400)',
  tableSelected: 'var(--theme-success-500)',
  tableSelectedBackground: 'var(--theme-success-500)',

  quote: 'var(--theme-elevation-400)',
  codeBackground: 'var(--theme-bg)',
  codeBorder: 'var(--theme-elevation-150)',
  horizontalRule: 'var(--theme-elevation-400)',
  imageErrorBackground: 'var(--theme-bg)',

  scrollbarBackground: 'var(--theme-bg)',
  scrollbarThumb: 'var(--theme-elevation-400)'
};

export default editorTheme;
