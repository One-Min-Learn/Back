import React from 'react';
import { useField } from 'payload/components/forms';
import { Label } from 'payload/components/forms';
import Editor from 'rich-markdown-editor';
import styled from 'styled-components';
import EditorTheme from './EditorTheme';

const StyledEditor = styled(Editor)`
  div[role='textbox'] {
    box-shadow: 0 2px 3px 0 rgb(0 2 4 / 5%), 0 10px 4px -8px rgb(0 2 4 / 2%);
    font-family: var(--font-body);
    width: 100%;
    border: 1px solid var(--theme-elevation-150);
    background: var(--theme-input-bg);
    color: var(--theme-elevation-800);
    border-radius: 0;
    font-size: 1rem;
    min-height: 3.8461538462rem;
    line-height: 1.9230769231rem;
    padding: 0.9615384615rem 1.4423076923rem;
    -webkit-appearance: none;

    &:hover {
      box-shadow: 0 2px 3px 0 rgb(0 2 4 / 13%), 0 6px 4px -4px rgb(0 2 4 / 10%);
    }

    &:focus {
      box-shadow: 0 2px 3px 0 rgb(0 2 4 / 16%), 0 6px 4px -4px rgb(0 2 4 / 13%);
    }

    .block-menu-trigger {
      display: flex;
      justify-content: center;
      align-items: center;
      margin-top: 0;
      margin-left: -32px;
      background: var(--theme-elevation-800);
      color: var(--theme-elevation-0);
      border-radius: 4px;

      &:hover,
      &:focus {
        transform: scale(1);
        background: var(--theme-elevation-750);
      }
    }

    .code-block.code-block select,
    .notice-block.code-block select,
    .code-block.code-block button,
    .notice-block.code-block button {
      right: 8px;
    }

    .code-block.notice-block select,
    .notice-block.notice-block select,
    .code-block.notice-block button,
    .notice-block.notice-block button {
      right: 8px;
    }

    .code-block select,
    .notice-block select,
    .code-block button,
    .notice-block button {
      background: transparent;
      color: currentColor;
      border-color: currentColor;
      border-radius: 0;
      top: 8px;
      outline: none;
    }
  }
`;

export default function EditorField({ path, label }) {
  const { value = '', setValue } = useField({ path });

  return (
    <div style={{ position: 'relative', marginBottom: '1.9230769231rem' }}>
      <Label htmlFor={path} label={label} />
      <StyledEditor
        id={`rich-markdown-editor-${path.replace(/W/g, '-')}`} // Any Non-Word Char
        defaultValue={value}
        onChange={newValue => setValue(newValue)}
        theme={EditorTheme}
      />
    </div>
  );
}
