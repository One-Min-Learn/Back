const Quotes = {
  slug: 'quotes',
  access: {
    read: () => true
  },
  fields: [
    {
      name: 'author',
      type: 'text'
    },
    {
      name: 'content',
      type: 'text'
    }
  ]
};

export default Quotes;
