import EditorField from '../components/EditorField';
import EditorCell from '../components/EditorCell';

const Snippets = {
  slug: 'snippets',
  access: {
    read: () => true
  },
  admin: {
    useAsTitle: 'title'
  },
  fields: [
    {
      name: 'title',
      type: 'text'
    },
    {
      name: 'content',
      type: 'text',
      admin: {
        components: {
          Field: EditorField,
          Cell: EditorCell
        }
      }
    },
    {
      name: 'categories',
      type: 'relationship',
      relationTo: 'categories',
      hasMany: true
    }
  ],
  hooks: {
    beforeRead: [
      args => {
        const { doc } = args;

        if (doc.content) {
          return {
            ...doc,
            // Prevent Rich MD Editor '\' Chars
            content: doc.content.replaceAll('\\\n', '\n')
          };
        }

        return doc;
      }
    ]
  }
};

export default Snippets;
