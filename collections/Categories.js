const Categories = {
  slug: 'categories',
  access: {
    read: () => true
  },
  admin: {
    useAsTitle: 'title'
  },
  fields: [
    {
      name: 'title',
      type: 'text'
    },
    {
      name: 'subTitle',
      type: 'text'
    }
  ]
};

export default Categories;
