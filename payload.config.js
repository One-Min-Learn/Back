import { buildConfig } from 'payload/config';
import Users from './collections/Users';
import Categories from './collections/Categories';
import Quotes from './collections/Quotes';
import Snippets from './collections/Snippets';

export default buildConfig({
  serverURL: 'http://localhost:3000',
  admin: {
    user: Users.slug
  },
  collections: [Users, Categories, Quotes, Snippets]
  // cors: '*'
});
